﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_4_1
{
    static class ConvertorTemperatura
    {
        static int celsiusKelvinDifference = 273;
        public static int ConvertCelsiusToKelvin(int celsius)
        {
            return celsius + celsiusKelvinDifference;
        }
        public static float ConvertCelsiulToFahrenheit(float celsius)
        {
            return ((celsius * 18) / 10) + 32;
        }
        public static int ConvertKelvinToCelsius(int kelvin)
        {
            return kelvin - celsiusKelvinDifference;
        }
    }
}
