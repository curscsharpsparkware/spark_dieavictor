﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_4_1
{
    class Money
    {
        public Money()
        {
            Amount = 0;
            Currency = "RON";
        }
        public Money(decimal amount, string currency = "")
        {
            Amount = amount;
            Currency = currency;
        }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string GetAmountWithCurrency()
        {
            return Amount + " " + Currency;

        }
        public void MultiplyAmount(decimal factor)
        {
            Amount *= factor;
        }
        ~Money()
        {
            Console.WriteLine("BOOOOM!");
        }
    }
}
