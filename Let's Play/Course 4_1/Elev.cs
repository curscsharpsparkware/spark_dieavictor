﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_4_1
{
    class Elev
    {
        
        public Elev (string nume, string prenume, int varsta)
        {
            Nume = nume;
            Prenume = prenume;
            Varsta = varsta;
        }
        public string Nume { get; set; }
        public string Prenume { get; set; }
        public int Varsta { get; set; }

    }
}
