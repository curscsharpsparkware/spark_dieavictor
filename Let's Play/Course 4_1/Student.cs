﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_4_1
{
    class Student : Persoana
    {
        public string Facultate { get; set; }
        public float Medie { get; set; }
        public void PrinteazaInformatiiComplete()
        {
            Console.WriteLine($"{Nume} {Prenume}, {CNP}, cu varsta {Varsta} ani este student al facultatii {Facultate}, avand media {Medie}");
        }
    }
}
