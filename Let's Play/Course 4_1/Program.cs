﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_4_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Persoana p1 = new Persoana();
            Persoana p2 = new Persoana("testnume", "testprenume");
            Persoana p3 = new Persoana("testnume2", "");
            Elev elev1 = new Elev("NumeElevA", "PrenumeElevA", 16);
            Console.WriteLine(elev1.Nume + " " + elev1.Prenume + " " + elev1.Varsta);
            Money salariuA = new Money();
            salariuA.Amount = 10;
            salariuA.Currency = "USD";
            Money salariuB = new Money(25.5m, "RON");
            salariuB.MultiplyAmount(1.25m);
            Console.WriteLine(salariuA.GetAmountWithCurrency());
            Console.WriteLine(salariuB.GetAmountWithCurrency());
            //int a = int.Parse(Console.ReadLine());
            Console.WriteLine("celcius?");
            Console.WriteLine(ConvertorTemperatura.ConvertCelsiusToKelvin(int.Parse(Console.ReadLine())));
            Student stud1 = new Student();
            Console.WriteLine("nume, prenume, cnp, varsta, facultate, medie");
            stud1.Nume = Console.ReadLine();
            stud1.Prenume = Console.ReadLine();
            stud1.CNP = Console.ReadLine();
            stud1.Varsta = int.Parse(Console.ReadLine());
            stud1.Facultate = Console.ReadLine();
            stud1.Medie = int.Parse(Console.ReadLine());
            stud1.PrinteazaInformatiiComplete();
            Console.ReadLine();
        }
    }
}
