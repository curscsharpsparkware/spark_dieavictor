﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_4_1
{
    class Persoana
    {
        private string cnp;
        private bool esteBarbat;
        public Persoana()
        {
            Varsta = 18;
        }
        public Persoana(string nume, string prenume)
        {
            Nume = nume;
            Prenume = prenume;
        }
        public string Nume { get; set; }
        public string Prenume { get; set; }
        public int Varsta { get; set; }

        public string CNP
        {
            get
            {
                return cnp;
            }
            set
            {
                cnp = value;
            }
        }

        public bool EsteBarbat
        {
            get
            {
                return esteBarbat;
            }
            set
            {
                esteBarbat = value;
            }
        }
    }
}
