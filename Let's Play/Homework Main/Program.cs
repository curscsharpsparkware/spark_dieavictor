﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Week_1.WeekOneProjects;

namespace Homework_Main
{
    public class HomeworkMain
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Week 1 Homework Projects");
            string whichOne;
            do
            {
                Console.WriteLine("Select a week (1-1) or Quit");
                whichOne = Console.ReadLine();
                if (short.TryParse(whichOne, out short weekNumber))
                {
                    switch (weekNumber)
                    {
                        case 1:
                            Week_1.WeekOneProjects.Main(null);
                            break;
                        default:
                            break;
                    }
                }
            }
            while (string.Equals(whichOne, "quit", StringComparison.CurrentCultureIgnoreCase) == false);
        }
    }
}
