﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_3_1
{
    public class Persoana
    {
        private string nume;
        private string prenume;
        private int varsta;
        private string cnp;
        private bool esteBarbat;
        public string adresa;
        public void PrintNume()
        {
            Console.WriteLine("My name is " + this.nume);
        }
        public void SetNumeSiPrenume(string numePersoana, string prenumePersoana)
        {
            nume = numePersoana;
            prenume = prenumePersoana;
        }
        //public void PrintNumeSiPrenume()
        //{
        //    Console.WriteLine(nume + "" + prenume);
        //}
        //public string GetNume ()
        //{
        //    return nume;
        //}
        public void PrintNumeComplet()
        {
            Console.WriteLine(GetNumeComplet());
        }

        public string GetNumeComplet()
        {
            return nume + " " + prenume;
        }

        public void SchimbaPrenumele(string noulPrenume)
        {
            prenume = noulPrenume;
        }
        public void SchimbaNumePrenumeCNP(string noulNume, string noulPrenume = "PRENUME", string noulCNP = "CNP")
        {
            nume = noulNume;
            prenume = noulPrenume;
            cnp = noulCNP;
        }
    }
}
