﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week_2
{
    public class WeekTwoProjects
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Week 1 Homework Projects");
            string whichOne;
            do
            {
                Console.WriteLine("Select a project to run (1) or Quit");
                whichOne = Console.ReadLine();
                if (short.TryParse(whichOne, out short projectNumber))
                {
                    switch (projectNumber)
                    {
                        case 1:
                            Project1();
                            break;
                        default:
                            break;
                    }
                }
            }
            while (string.Equals(whichOne, "quit", StringComparison.CurrentCultureIgnoreCase) == false);
        }
        public static void Project1()
        {
            Console.WriteLine("Convering a Arabic Number to Roman Numberals");
            string whichOne;
            do
            {
                Console.WriteLine("What do you want to do");
                Console.WriteLine("Convert a number or Quit?");
                whichOne = Console.ReadLine();
                if (string.Equals(whichOne, "convert", StringComparison.CurrentCultureIgnoreCase) == true)
                {
                    Console.WriteLine("What's your number? (integer between 0 and 3.999.999)");
                    string isANumber = Console.ReadLine();
                    if (int.TryParse(isANumber, out int numberWhole) && numberWhole >= 0 && numberWhole <= 39999999)
                    {
                        string result = ArabicToRoman(numberWhole);
                        Console.WriteLine(result);
                    }
                    else
                    {
                        Console.WriteLine($"{isANumber} is not an integer between 0 and 3.999.999");
                    }
                }
            }
            while (string.Equals(whichOne, "quit", StringComparison.CurrentCultureIgnoreCase) == false);

        }
        public static string ArabicToRoman(int arabic)
        {

            string[] Thousands = { "", "M", "MM", "MMM" };
            string[] Hundreds = { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" };
            string[] Tens = { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" };
            string[] Ones = { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };
            string result = "";
            int number;

            if (arabic >= 4000) //values oves 3999 are displayed by using a value under 3999 multipled by 1000 + the remaining value. ex 4001 = 4 x 1000 + 1 = (IV)I 
            {
                int overflow = arabic / 1000;
                arabic %= 1000;
                return "(" + ArabicToRoman(overflow) + ")" + ArabicToRoman(arabic);
            }

            number = arabic / 1000;
            result += Thousands[number];
            arabic %= 1000;

            number = arabic / 100;
            result += Hundreds[number];
            arabic %= 100;

            number = arabic / 10;
            result += Tens[number];
            arabic %= 10;

            result += Ones[arabic];

            return result;
        }
    }
}
