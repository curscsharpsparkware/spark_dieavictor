﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week_1
{
    public class WeekOneProjects
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Week 1 Homework Projects");
            string whichOne;
            do
            {
                Console.WriteLine("Select a project to run (1-6) or Quit");
                whichOne = Console.ReadLine();
                if (short.TryParse(whichOne, out short projectNumber))
                {
                    switch (projectNumber)
                    {
                        case 1:
                            Project1();
                            break;
                        case 2:
                            Project2();
                            break;
                        case 3:
                            Project3();
                            break;
                        case 4:
                            Project4();
                            break;
                        case 5:
                            Project5();
                            break;
                        case 6:
                            Project6();
                            break;
                        default:
                            break;
                    }
                }
            }
            while (string.Equals(whichOne, "quit", StringComparison.CurrentCultureIgnoreCase) == false);
        }
        public static void Project1()
        {
            Console.WriteLine("Curs C# - Proiectul 2");
            int var_a;
            var_a = -5;
            Console.WriteLine($"Valoarea lui var_a este: {var_a}");
            bool var_b = true;
            var_b = var_a > 0;
            Console.WriteLine($"Var_a este mai care ca 0: {var_b}");
            var_a = var_a + 20;
            var_a = var_a / 6;
            Console.WriteLine($"Noua valoare a lui var_a este: {var_a}");
            float var_c = 1.25f;
            var_c = var_c * var_a;
            Console.WriteLine($"Noua valoare a lui var_c este: {var_c}");
            //short var_d = 40000; //short variable does not support value
            uint var_d; //this is the first declaration of var_d
            //var_d = -10; //unsigned int variable do not suport values < 0
            long var_e = 10, var_f, var_g = 11;
            bool var_h = false; char var_i = 'B';
        }
        public static void Project2()
        {
            Console.WriteLine("Curs C# - Proiectul 3");
            short var_a = short.Parse(Console.ReadLine()), var_b = short.Parse(Console.ReadLine()), var_c = short.Parse(Console.ReadLine());
            Console.WriteLine($"Suma celor 3 numere este: {var_a + var_b + var_c}");
            Console.WriteLine($"{var_a}X{var_b} = {var_a * var_b}");
            Console.WriteLine($"Catul {var_c}/{var_a}={var_c / var_a}");
            Console.WriteLine($"Resul {var_a}/{var_b}={var_a % var_b}");
            var_a += var_b; var_a -= var_c;
            Console.WriteLine($"Noua valoare a lui var_a este: {var_a}");
            bool var_d = ((var_a > 0) && (var_b > 0) && (var_c > 0));
            Console.WriteLine($"var_a, var_b, var_c sunt toate pozitive: {var_d}");
            bool var_e = ((var_a > 0) || (var_b > 0));
            Console.WriteLine($"Cel putin unu dintre var_a si var_b sunt pozitive: {var_e}");
            var_a++;
            var_a *= 5;
            Console.WriteLine($"Noua valoare a lui var_a este: {var_a}");
        }
        public static void Project3()
        {
            Console.WriteLine("Curs C# - Proiectul 4");
            string whichOne;
            do
            {
                Console.WriteLine("What do you want to do");
                Console.WriteLine("Calculate a Square, a Circle a Sphere or Quit?");
                whichOne = Console.ReadLine();
                string isANumber;
                if (whichOne == "Square" || whichOne == "square")
                {
                    Console.WriteLine("Lenght of the side of a square: ");
                    isANumber = Console.ReadLine();
                    if (float.TryParse(isANumber, out float sideSquare) && sideSquare > 0)
                    {
                        float perimeterSquare = sideSquare * 4; //Perimeter of square: side*4
                        Console.WriteLine($"The perimeter of the square is {perimeterSquare}");
                        float areaSquare = sideSquare * sideSquare; //Area of a square: side*side
                        Console.WriteLine($"The area of the square is {areaSquare}");
                    }
                    else
                    {
                        Console.WriteLine($"{isANumber} is not number greater than 0!");
                    }
                }
                else
                {
                    if (whichOne == "Circle" || whichOne == "circle")
                    {
                        Console.WriteLine("Diameter of a circle: ");
                        isANumber = Console.ReadLine();
                        if (float.TryParse(isANumber, out float diameterCircle) && diameterCircle > 0)
                        {
                            double perimeterCircle = 2 * Math.PI * (diameterCircle / 2); //Perimeter of a circle: 2 * Pi * radius
                            Console.WriteLine($"The perimeter of the circle is {perimeterCircle}");
                            double areaCircle = Math.PI * (diameterCircle / 2) * (diameterCircle / 2); //Area of a circle: Pi * radius * radius
                            Console.WriteLine($"The area of the circle is {areaCircle}");
                        }
                        else
                        {
                            Console.WriteLine($"{isANumber} is not number greater than 0!");
                        }
                    }
                    else
                    {
                        if (whichOne == "Sphere" || whichOne == "sphere")
                        {
                            Console.WriteLine("Radius of a sphere: ");
                            isANumber = Console.ReadLine();
                            if (float.TryParse(isANumber, out float radiusSphere) && radiusSphere > 0)
                            {
                                double areaSphere = 4 * Math.PI * radiusSphere * radiusSphere; //Area of a sphere: 4 * Pi * radius * radius
                                Console.WriteLine($"The area of the sphere is {areaSphere}");
                                double volumeSphere = 4 / 3 * Math.PI * radiusSphere * radiusSphere * radiusSphere; //Volume of a sphere: 4 / 3 * Pi * radius * radius * radius
                                Console.WriteLine($"The volume of the sphere is {volumeSphere}");
                            }
                            else
                            {
                                Console.WriteLine($"{isANumber} is not number greater than 0!");
                            }
                        }
                    }
                }
            }
            while (string.Equals(whichOne, "quit", StringComparison.CurrentCultureIgnoreCase) == false);
        }
        public static void Project4()
        {
            Console.WriteLine("Curs C# - Proiectul 5");
            string whichOne;
            do
            {
                Console.WriteLine("What do you want to do");
                Console.WriteLine("Decompose a number or Quit?");
                whichOne = Console.ReadLine();
                if (whichOne == "Decompose" || whichOne == "decompose")
                {
                    Console.WriteLine("What's your number?");
                    string isANumber = Console.ReadLine();
                    if (int.TryParse(isANumber, out int numberWhole))
                    {
                        if ((numberWhole / 10000) != 0 && (numberWhole / 10000) <= 9)
                        {
                            Console.WriteLine($"Breakdown of {numberWhole} is {numberWhole / 10000} x 10.000 + {numberWhole % 10000 / 1000} x 1.000 + {numberWhole % 1000 / 100} x 100 + {numberWhole % 100 / 10} x 10 + {numberWhole % 10} x 1.");
                        }
                        else
                        {
                            Console.WriteLine($"{numberWhole} does not contain 5 digits");
                        }
                    }
                    else
                    {
                        Console.WriteLine($"{isANumber} is not an integer");
                    }
                }
            }
            while (string.Equals(whichOne, "quit", StringComparison.CurrentCultureIgnoreCase) == false);
        }
        public static void Project5()
        {
            Console.WriteLine("Curs C# - Proiectul 6");
            string whichOne;
            do
            {
                Console.WriteLine("What do you want to do");
                Console.WriteLine("Switch a number or Quit?");
                whichOne = Console.ReadLine();
                if (whichOne == "Switch" || whichOne == "switch")
                {
                    Console.WriteLine("What's the first number?");
                    string isANumber = Console.ReadLine();
                    if (int.TryParse(isANumber, out int firstNumber))
                    {
                        Console.WriteLine("What's the second number?");
                        isANumber = Console.ReadLine();
                        if (int.TryParse(isANumber, out int secondNumber))
                        {
                            Console.WriteLine($"The first number is: {firstNumber}");
                            Console.WriteLine($"The second number is: {secondNumber}");
                            int switchNumber = firstNumber;
                            firstNumber = secondNumber;
                            secondNumber = switchNumber;
                            Console.WriteLine($"The first number after the switch is: {firstNumber}");
                            Console.WriteLine($"The second number after the switch is: {secondNumber}");

                        }
                        else
                        {
                            Console.WriteLine($"{isANumber} is not an integer!");
                        }
                    }
                    else
                    {
                        Console.WriteLine($"{isANumber} is not an integer!");
                    }
                }
            }
            while (string.Equals(whichOne, "quit", StringComparison.CurrentCultureIgnoreCase) == false);
        }
        public static void Project6()
        {
            Console.WriteLine("Curst C# - Proiectul 7");
            string whichOne;
            do
            {
                Console.WriteLine("What do you want to me?");
                Console.WriteLine("Multiplication table or Quit?");
                whichOne = Console.ReadLine();
                if (whichOne == "Multiplication" || whichOne == "multiplication")
                {
                    Console.WriteLine("What's the first number? (1-10)");
                    string isANumber = Console.ReadLine();
                    if (short.TryParse(isANumber, out short firstNumber) && firstNumber > 0 && firstNumber < 11)
                    {
                        Console.WriteLine("What's the second number? (1-10)");
                        isANumber = Console.ReadLine();
                        if (short.TryParse(isANumber, out short secondNumber) && secondNumber > 0 && secondNumber < 11)
                        {
                            for (short i = 1; i <= firstNumber; i++)
                            {
                                for (short j = 1; j <= secondNumber; j++)
                                {
                                    Console.WriteLine($"{i} X {j} = {i * j}");
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine($"{isANumber} is not an integer between 1 and 10");
                        }
                    }
                    else
                    {
                        Console.WriteLine($"{isANumber} is not an integer between 1 and 10");
                    }
                }

            }
            while (string.Equals(whichOne, "quit", StringComparison.CurrentCultureIgnoreCase) == false);
        }
    }
}
